package ch.stgerb.flappybird.model

data class HighScoresListItem(val score: Long, val dateTime: String)