package ch.stgerb.flappybird.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ch.stgerb.flappybird.R
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(), View.OnClickListener
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        button_play.setOnClickListener(this)
        button_high_scores.setOnClickListener(this)
    }

    override fun onClick(view: View)
    {
        when (view.id)
        {
            R.id.button_play -> replaceFragment(GetReadyFragment())
            R.id.button_high_scores -> replaceFragment(HighScoresFragment())
        }
    }

    fun replaceFragment(fragment: Fragment)
    {
        activity!!.supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }
}