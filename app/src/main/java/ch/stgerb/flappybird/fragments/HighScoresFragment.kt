package ch.stgerb.flappybird.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import ch.stgerb.flappybird.R
import ch.stgerb.flappybird.io.ScoreSafer
import ch.stgerb.flappybird.model.HighScoresListItem
import kotlinx.android.synthetic.main.fragment_high_scores.*
import org.json.simple.JSONObject
import java.io.File
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList


class HighScoresFragment : Fragment(), View.OnClickListener
{
    private val mHighScoresList = ArrayList<HighScoresListItem>()
    private val mScoreSafer = ScoreSafer()
    private lateinit var mFile: File
    private lateinit var mAdapter: HighScoresListAdapter

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        mFile = File(activity!!.filesDir, "scores.json")
        val jsonArray = mScoreSafer.readJSONFile(mFile)

        if (!jsonArray.isEmpty())
        {
            for (value in jsonArray)
            {
                val jsonObject = value as JSONObject
                val score = jsonObject["score"] as Long
                val dateTime = jsonObject["dateTime"] as String
                val highScoresListItem = HighScoresListItem(score, dateTime)

                mHighScoresList.add(highScoresListItem)
            }

            Collections.sort(mHighScoresList, object : Comparator<HighScoresListItem>
            {
                override fun compare(s1: HighScoresListItem?, s2: HighScoresListItem?): Int
                {
                    return (s2!!.score).compareTo(s1!!.score)
                }
            })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_high_scores, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        if (!mHighScoresList.isEmpty())
        {
            text_empty_listview.visibility = View.GONE

            mAdapter = HighScoresListAdapter(activity!!, R.layout.high_scores_list_item, mHighScoresList)
            listview_high_scores.adapter = mAdapter
        }
        else
        {
            button_clear_list.hide()
        }

        button_clear_list.setOnClickListener(this)
    }

    private class HighScoresListAdapter(context: Context, val resource: Int, list: ArrayList<HighScoresListItem>) : ArrayAdapter<HighScoresListItem>(context, resource, list)
    {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View
        {
            val viewHolder: ViewHolder
            val retView: View

            if (convertView == null)
            {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                retView = inflater.inflate(resource, parent, false)

                viewHolder = ViewHolder()

                viewHolder.textScore = retView.findViewById(R.id.text_score)
                viewHolder.textDateTime = retView.findViewById(R.id.text_date_time)

                retView.tag = viewHolder
            }
            else
            {
                viewHolder = convertView.tag as ViewHolder
                retView = convertView
            }

            viewHolder.textScore.setText(getItem(position).score.toString())
            viewHolder.textDateTime.setText(getItem(position).dateTime)

            return retView
        }

        private class ViewHolder
        {
            lateinit var textScore: TextView
            lateinit var textDateTime: TextView
        }
    }

    override fun onClick(view: View?)
    {
        mScoreSafer.deleteJSONFile(mFile)

        mHighScoresList.clear()
        mAdapter.notifyDataSetChanged()

        button_clear_list.hide()
        text_empty_listview.visibility = View.VISIBLE
    }
}