package ch.stgerb.flappybird.interfaces

interface CollisionListener
{
    fun onCollision(score: Long)
}