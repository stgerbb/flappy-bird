package ch.stgerb.flappybird.io

import android.util.Log
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException

class ScoreSafer
{
    fun createJSONObject(score: Long, dateTime: String): JSONObject
    {
        val jsonObject = JSONObject()

        jsonObject.put("score", score)
        jsonObject.put("dateTime", dateTime)

        return jsonObject
    }

    fun writeToFile(jsonObject: JSONObject, file: File)
    {
        if (file.exists())
        {
            val jsonArray = readJSONFile(file)
            jsonArray.add(jsonObject)

            val fileWriter = FileWriter(file)

            try
            {
                fileWriter.write(jsonArray.toJSONString())
            }
            catch (e: IOException)
            {
                Log.i("Log", e.message)
            }
            finally
            {
                fileWriter.flush()
                fileWriter.close()
            }
        }
        else
        {
            val fileWriter = FileWriter(file)

            try
            {
                fileWriter.write(jsonObject.toJSONString())
            }
            catch (e: IOException)
            {
                Log.i("Log", e.message)
            }
            finally
            {
                fileWriter.flush()
                fileWriter.close()
            }
        }
    }

    fun readJSONFile(file: File): JSONArray
    {
        if (file.exists())
        {
            val content = JSONParser().parse(FileReader(file))

            if (content.toString().contains("["))
            {
                return content as JSONArray
            }
            else
            {
                val jsonObject = content as JSONObject
                val jsonArray = JSONArray()
                jsonArray.add(jsonObject)

                return jsonArray
            }
        }
        
        return JSONArray()
    }

    fun  deleteJSONFile(file: File)
    {
        file.delete()
    }
}